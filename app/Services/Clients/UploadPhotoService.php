<?php

namespace App\Services\Clients;

use Illuminate\Http\UploadedFile;

class UploadPhotoService
{
    public function run(UploadedFile $uploadedFile)
    {
        return $uploadedFile->store('app/public/clients/photos');
    }
}
