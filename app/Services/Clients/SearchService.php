<?php

namespace App\Services\Clients;

use App\Repositories\ClientRepository;

class SearchService
{
    /**
     * @var ClientRepository
     */
    private $clientRepository;

    /**
     * SearchService constructor.
     * @param ClientRepository $clientRepository
     */
    public function __construct(ClientRepository $clientRepository)
    {

        $this->clientRepository = $clientRepository;
    }

    /**
     * @return mixed
     */
    public function run()
    {
        return $this->clientRepository->all();
    }
}
