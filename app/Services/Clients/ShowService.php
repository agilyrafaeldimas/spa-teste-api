<?php

namespace App\Services\Clients;

use App\Entities\Client;
use App\Repositories\ClientRepository;

class ShowService
{
    /**
     * @var ClientRepository
     */
    private $clientRepository;

    /**
     * ShowService constructor.
     * @param ClientRepository $clientRepository
     */
    public function __construct(ClientRepository $clientRepository)
    {
        $this->clientRepository = $clientRepository;
    }

    /**
     * @param int $id
     * @return Client
     */
    public function run(int $id): Client
    {
        return $this->clientRepository->find($id);
    }
}
