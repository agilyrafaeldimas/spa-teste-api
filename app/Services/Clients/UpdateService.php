<?php

namespace App\Services\Clients;

use App\Entities\Client;
use App\Repositories\ClientRepository;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Arr;

class UpdateService
{
    /**
     * @var ClientRepository
     */
    private $clientRepository;

    /**
     * @var UploadPhotoService
     */
    private $uploadPhotoService;

    /**
     * StoreService constructor.
     * @param ClientRepository $clientRepository
     * @param UploadPhotoService $uploadPhotoService
     */
    public function __construct(ClientRepository $clientRepository, UploadPhotoService $uploadPhotoService)
    {
        $this->clientRepository = $clientRepository;
        $this->uploadPhotoService = $uploadPhotoService;
    }

    /**
     * @param int $id
     * @param array $data
     * @return Client
     */
    public function run(int $id, array $data): Client
    {
        /** @var UploadedFile|null $photo */
        if ($photo = Arr::get($data, 'photo')) {
            Arr::set($data, 'photo', $this->uploadPhotoService->run($photo) ?: null);
        }

        /** @var Client $client */
        $client = $this->clientRepository->update($data, $id);

        return $client;
    }
}
