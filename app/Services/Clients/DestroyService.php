<?php

namespace App\Services\Clients;

use App\Entities\Client;
use App\Repositories\ClientRepository;

class DestroyService
{
    /**
     * @var ClientRepository
     */
    private $clientRepository;

    /**
     * StoreService constructor.
     * @param ClientRepository $clientRepository
     */
    public function __construct(ClientRepository $clientRepository)
    {
        $this->clientRepository = $clientRepository;
    }

    /**
     * @param int $id
     * @return bool
     */
    public function run(int $id)
    {
        return (bool) $this->clientRepository->delete($id);
    }
}
