<?php

namespace App\Http\Controllers;

use App\Http\Requests\ClientUpdateRequest;
use App\Services\Clients\DestroyService;
use App\Services\Clients\SearchService;
use App\Services\Clients\ShowService;
use App\Services\Clients\StoreService;
use App\Services\Clients\UpdateService;
use Illuminate\Http\JsonResponse;
use App\Http\Requests\ClientCreateRequest;

/**
 * Class ClientsController.
 *
 * @package namespace App\Http\Controllers;
 */
class ClientsController extends Controller
{
    /**
     * @param SearchService $searchService
     * @return JsonResponse
     */
    public function index(SearchService $searchService): JsonResponse
    {
        $data = $searchService->run();
        return response()->json($data);
    }

    /**
     * @param StoreService $storeService
     * @param ClientCreateRequest $clientCreateRequest
     * @return JsonResponse
     */
    public function store(StoreService $storeService, ClientCreateRequest $clientCreateRequest): JsonResponse
    {
        $data = $clientCreateRequest->validated();

        $client = $storeService->run($data);

        return response()->json($client, 201);
    }

    /**
     * @param ShowService $showService
     * @param int $id
     * @return JsonResponse
     */
    public function show(ShowService $showService, int $id): JsonResponse
    {
        $client = $showService->run($id);
        return response()->json($client);
    }

    /**
     * @param UpdateService $updateService
     * @param ClientUpdateRequest $clientUpdateRequest
     * @param int $id
     * @return JsonResponse
     */
    public function update(UpdateService $updateService, ClientUpdateRequest $clientUpdateRequest, int $id): JsonResponse
    {
        $data = $clientUpdateRequest->validated();

        $client = $updateService->run($id, $data);

        return response()->json($client);
    }

    /**
     * @param DestroyService $destroyService
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(DestroyService $destroyService, int $id): JsonResponse
    {
        $destroyed = $destroyService->run($id);
        return response()->json(compact('destroyed'));
    }
}
